libapache2-mod-lisp for Debian 
-----------------------------

Please visit http://www.fractalconcept.com/asp/mod_lisp for more
information.

The package does not do any automatic configuration.  Please configure
Apache2 appropriately, according to the following instructions.

Configuration
-------------

In the Apache2 config file "/etc/apache2/apache2.conf":

Add a LispServer directive:

    LispServer IP-ADDRESS IP-PORT USER-DEFINED-NAME

where

    IP-ADDRESS is the ip address of the computer where the Lisp
    process is.

    IP-PORT is the ip port of the computer where the Lisp process is.

    USER-DEFINED-NAME is a string that will be passed from Apache to
    the Lisp process. It is useful when you have several virtual
    servers or directories, defined within an Apache server, to be
    processed by mod_lisp.

For example:

    LispServer 127.0.0.1 3000 "fractal"

Then add a SetHandler directive:

    <Location /asp> 
	SetHandler lisp-handler 
    </Location> 

All of the URLs located in /asp (e.g. "/asp/index.html") will be sent to
the Lisp process.

It is recommended that you wrap the above directives in an IfModule
block, like this:

    <IfModule mod_lisp.c>
	LispServer  127.0.0.1 3000 "fractal" 
	...  
	<Location /asp> 
	    SetHandler lisp-handler 
	</Location> 
    </IfModule>

After you having finished configuration, restart Apache2.
